package app.core.bff;

public class SearchResultsFood {

	private String company = "";
	private String booth_number = "";
	private String location = "";
	private String cuisine = "";
	
	public void setCompany(String company) {
		this.company = company;
	}

	public String getCompany() {
		return company ;
	}

	public void setBoothNumber(String booth_number) {
		this.booth_number = booth_number;
	}

	public String getBoothNumber() {
		return booth_number;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLocation() {
		return location;
	}
	
	public void setCuisine(String cuisine) {
		this.cuisine = cuisine;
	}

	public String getCuisine() {
		return cuisine;
	}
		
}