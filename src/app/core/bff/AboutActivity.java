package app.core.bff;

import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class AboutActivity extends Activity implements OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE); 
        setContentView(R.layout.activity_about);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
        
        TextView actTv = (TextView) findViewById(R.id.titleHeading);
        actTv.setText(R.string.title_activity_about);
        findViewById(R.id.openMenu).setOnClickListener(this);
        findViewById(R.id.openSearch).setOnClickListener(this);
        findViewById(R.id.button1).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
        findViewById(R.id.button3).setOnClickListener(this);
    }
    
	@Override
	public void onResume() {
		super.onResume();
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    @Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.openMenu) {
			openMenuActivity();
		} else if (id == R.id.openSearch) {
			startActivity(new Intent(AboutActivity.this, SearchActivity.class));//open search activity, THEN open search dialog
		} else if (id == R.id.button1) {
			openGoogleMaps();
		} else if (id == R.id.button2) {
			openBigFairFun();
		} else if (id == R.id.button3) {
			openCreditsActivity();
		} else {
		}
	}
    
    private void openGoogleMaps()
    {
    	String uri = String.format(Locale.ENGLISH, "geo:0,0?q=873+NE+34th+Ave+Hillsboro+OR+97124");
    	Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
    	startActivity(intent);
    }
    
    private void openBigFairFun()
    {
    	Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://bigfairfun.com/mobile/index.php"));
		startActivity(browserIntent);
    }
    
    private void openCreditsActivity(){
		startActivity(new Intent(AboutActivity.this, CreditsActivity.class));
    }  
    
    private void openMenuActivity(){
		int width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
		SlideoutActivity.prepare(AboutActivity.this, R.id.inner_content, width);
		startActivity(new Intent(AboutActivity.this, MenuActivity.class));
		overridePendingTransition(0, 0);
    }  
   
	
}
