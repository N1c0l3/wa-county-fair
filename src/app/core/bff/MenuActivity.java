package app.core.bff;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.Window;

public class MenuActivity extends FragmentActivity{

	//@TargetApi(17)
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    /*if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
	    	getActionBar().hide();
	    }*/
	    mSlideoutHelper = new SlideoutHelper(this);
	    mSlideoutHelper.activate();
	    getSupportFragmentManager().beginTransaction().add(R.id.slideout_placeholder, new MenuFragment(), "menu").commit();
	    mSlideoutHelper.open();
	}

	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			mSlideoutHelper.close();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	public void favoritesClick()
	{
		Intent myIntent = new Intent(MenuActivity.this, FavoritesActivity.class);
 		MenuActivity.this.startActivity(myIntent);
	}
	
	public void mapClick()
	{
		Intent myIntent = new Intent(MenuActivity.this, MapActivity.class);
 		MenuActivity.this.startActivity(myIntent);
	}
	
	public void scheduleClick()
	{
		Intent myIntent = new Intent(MenuActivity.this, ScheduleActivity.class);
 		MenuActivity.this.startActivity(myIntent);
	}
	
	public void carnivalClick()
	{
		Intent myIntent = new Intent(MenuActivity.this, CarnivalActivity.class);
 		MenuActivity.this.startActivity(myIntent);
	}
	
	public void foodClick()
	{
		Intent myIntent = new Intent(MenuActivity.this, FoodActivity.class);
 		MenuActivity.this.startActivity(myIntent);
	}
	
	public void shoppingClick()
	{
		Intent myIntent = new Intent(MenuActivity.this, ShoppingActivity.class);
 		MenuActivity.this.startActivity(myIntent);
	}
	
	public void aboutClick()
	{
		Intent myIntent = new Intent(MenuActivity.this, AboutActivity.class);
 		MenuActivity.this.startActivity(myIntent);
	}
	
	public void searchClick()
	{
		Intent myIntent = new Intent(MenuActivity.this, SearchActivity.class);
 		MenuActivity.this.startActivity(myIntent);
	}

	public SlideoutHelper getSlideoutHelper(){
		return mSlideoutHelper;
	}
	
	private SlideoutHelper mSlideoutHelper;

}