package app.core.bff;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MenuFragment extends ListFragment {

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setListAdapter(new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1, new String[] { "General Info", "Events", "Carnival", "Shopping", "Food", "Map", "Favorites", "Search"}));
		//This is where to add links to views
		getListView().setCacheColorHint(0);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		switch(position){
		case 0:
			aboutClick();
			break;
		case 1:
			scheduleClick();
			break;
		case 2:
			carnivalClick();
			break;
		case 3:
			shoppingClick();
			break;
		case 4:
			foodClick();
			break;
		case 5:
			mapClick();
			break;
		case 6:
			favoritesClick();
			break;
		case 7:
			searchClick();
			break;	
		}
		((MenuActivity)getActivity()).getSlideoutHelper().close();
	}
	
	public void favoritesClick()
	{
		((MenuActivity)getActivity()).favoritesClick();
	}
	
	public void searchClick()
	{
		((MenuActivity)getActivity()).searchClick();
	}
	
	public void mapClick()
	{
		((MenuActivity)getActivity()).mapClick();
	}
	
	public void scheduleClick()
	{
		((MenuActivity)getActivity()).scheduleClick();
	}
	
	public void carnivalClick()
	{
		((MenuActivity)getActivity()).carnivalClick();
	}
	
	public void foodClick()
	{
		((MenuActivity)getActivity()).foodClick();
	}
	
	public void shoppingClick()
	{
		((MenuActivity)getActivity()).shoppingClick();
	}
	
	public void aboutClick()
    {
   		((MenuActivity)getActivity()).aboutClick();
    }

	
}
