package app.core.bff;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomAdapterSearch extends BaseAdapter {
	private static ArrayList<SearchResults> searchArrayList;
	private Context myContext;
	
	private LayoutInflater mInflater;

	public CustomAdapterSearch(Context context, ArrayList<SearchResults> results) {
		searchArrayList = results;
		mInflater = LayoutInflater.from(context);
		myContext = context;
	}
	
	public int getCount() {
		return searchArrayList.size();
	}

	public Object getItem(int position) {
		return searchArrayList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.search_layout, null);
			holder = new ViewHolder();
			holder.checkBox = (CheckBox) convertView.findViewById(R.id.favbutton);
			holder.image = (ImageView) convertView.findViewById(R.id.icon);
			holder.column1 = (TextView) convertView.findViewById(R.id.column1);
			holder.column2 = (TextView) convertView.findViewById(R.id.column2);
			holder.column3 = (TextView) convertView.findViewById(R.id.column3);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		String temp = searchArrayList.get(position).getImage();
		temp = temp.substring(0, temp.length()-4);//getIdentifier only uses the base file name, not the '.png' suffix
		int resId = myContext.getResources().getIdentifier(temp, "drawable", "app.core.bff");//this code should be checked
		holder.image.setImageResource(resId);
		if(searchArrayList.get(position).getType().equals("event"))
		{			
			String dayAndTime = searchArrayList.get(position).getColumn2() + " from "
					+ searchArrayList.get(position).getColumn3() + " to "
					+ searchArrayList.get(position).getColumn5();
			holder.column1.setText(searchArrayList.get(position).getColumn1());
            holder.column2.setText(dayAndTime);
            holder.column3.setText(searchArrayList.get(position).getColumn4());
		}
		else if(searchArrayList.get(position).getType().equals("food"))
		{
			holder.column1.setText(searchArrayList.get(position).getColumn1());
			holder.column2.setText(searchArrayList.get(position).getColumn4());
			holder.column3.setText(searchArrayList.get(position).getColumn3());
		}
		else if(searchArrayList.get(position).getType().equals("commercial"))
		{
			holder.column1.setText(searchArrayList.get(position).getColumn1());
			holder.column2.setText(searchArrayList.get(position).getColumn3());
			holder.column3.setText(searchArrayList.get(position).getColumn4());
		}
		else if(searchArrayList.get(position).getType().equals("carnival"))
		{
			holder.column1.setText(searchArrayList.get(position).getColumn1());
			holder.column2.setText("Coupons:  " + searchArrayList.get(position).getColumn2()
					+ "     Min Height: " + searchArrayList.get(position).getColumn3());
			holder.column3.setText(searchArrayList.get(position).getColumn4());
		}
		
		SQLiteAdapter myDbHelper = new SQLiteAdapter(myContext);
		Integer id = searchArrayList.get(position).getId();
		String type = searchArrayList.get(position).getType();
		holder.checkBox.setChecked(myDbHelper.isFavorite(type, id));

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View v) {
        		SQLiteAdapter myDbHelper = new SQLiteAdapter(myContext);
        		Integer id = searchArrayList.get(position).getId();
        		String type = searchArrayList.get(position).getType();
        		myDbHelper.setIsFavorite(type, id, ((CheckBox)v).isChecked());
        	}
        }); 
		
		return convertView;
	}

	static class ViewHolder {
		TextView column1;
		TextView column2;
		TextView column3;
		CheckBox checkBox;
		ImageView image;
	}
}