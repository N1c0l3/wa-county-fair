package app.core.bff;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;


public class JSONParser extends AsyncTask<String, Void, JSONObject> {

	static InputStream is= null;
	static JSONObject jsonObj = null;
	static String jsonResponse = "";
	
	public JSONParser() {
		
	}

	protected JSONObject doInBackground(String... params) {
		try {
			String response = new JSONObject().toString();
			URL	thisUrl = new URL(params[0].toString());
			HttpURLConnection urlConnection = (HttpURLConnection) thisUrl.openConnection(); 
			//urlConnection.setRequestProperty("lastUpdatedTime", 0);
//			urlConnection.setReadTimeout(10000);
//			urlConnection.setConnectTimeout(15000);
			//urlConnection.setRequestMethod("GET");
//			urlConnection.setDoInput(true);
//			urlConnection.setDoOutput(true);
			//urlConnection.setFixedLengthStreamingMode(response.getBytes().length);
			urlConnection.setRequestProperty("Accept", "application/json");
		
			
			//urlConnection.connect();
			if (urlConnection.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ urlConnection.getResponseCode());
			}
//			url = url + "?lastTimeUpdated=0";
			try {
			     //InputStream in = new BufferedInputStream(urlConnection.getInputStream());
			     BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
			     StringBuilder sb = new StringBuilder();
			     String line = null;
			     while ((line = reader.readLine()) != null) {
			    	 sb.append(line);
			    	 }
			     reader.close();
			     jsonResponse = sb.toString();
			}
			finally {
				urlConnection.disconnect();
				}
//			HttpGet httpGet = new HttpGet(thisUrl);
//			HttpResponse httpResponse = httpClient.execute(httpGet);
//			HttpEntity httpEntity = httpResponse.getEntity();
//			is = httpEntity.getContent();
			} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			} catch (ClientProtocolException e) {
			e.printStackTrace();
			} catch (IOException e) { 
			e.printStackTrace();
			}
//			} catch (Exception e) {
//			Log.e("Buffer Error", "Error Converting Result " + e.toString());
//		}
				
				try {
					jsonObj = new JSONObject(jsonResponse);
				} catch (JSONException e) {
					Log.e("JSONParser", "Error parsing response data " + e.toString());
				}
				return jsonObj;
	}

}
