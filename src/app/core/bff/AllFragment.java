package app.core.bff;

import java.util.ArrayList;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.view.ViewGroup;

public class AllFragment extends Fragment {
	private CustomBaseAdapter adapter = null;
	static private ArrayList<SearchResults> allEventResults = new ArrayList<SearchResults>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.allfragment, container, false);
        
    	ListView lv1 = (ListView) view.findViewById(R.id.schedule1);
    	adapter = new CustomBaseAdapter(getActivity(), allEventResults);
    	lv1.setAdapter(adapter);

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent detailIntent = new Intent(getActivity(), DetailViewActivity.class);

                CustomBaseAdapter ca = (CustomBaseAdapter)parent.getAdapter();
                SearchResults sr = (SearchResults)ca.getItem(position);

                detailIntent.putExtra("details", sr.getDetails());
                startActivity(detailIntent);
            }
        });
    	return view;
    }
	@Override
	public void onResume() {
		super.onResume();
		SQLiteAdapter myDbHelper = new SQLiteAdapter(null);
		ArrayList<SearchResults> localEventResults = myDbHelper.getAllDayEvents();
		allEventResults.clear();
		allEventResults.addAll(localEventResults);
		adapter.notifyDataSetChanged();
	}
}