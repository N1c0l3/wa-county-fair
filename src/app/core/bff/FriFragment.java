package app.core.bff;

import java.util.ArrayList;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

public class FriFragment extends Fragment {
	static private ArrayList<SearchResults> friEventResults = new ArrayList<SearchResults>();
	private CustomBaseAdapter adapter = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.frifragment, container, false);
        
    	ListView lv3 = (ListView) view.findViewById(R.id.schedule3);
    	adapter = new CustomBaseAdapter(getActivity(), friEventResults);
    	lv3.setAdapter(adapter);

        lv3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent detailIntent = new Intent(getActivity(), DetailViewActivity.class);

                CustomBaseAdapter ca = (CustomBaseAdapter)parent.getAdapter();
                SearchResults sr = (SearchResults)ca.getItem(position);

                detailIntent.putExtra("details", sr.getDetails());
                startActivity(detailIntent);
            }
        });
    	return view;
    }

    @Override
	public void onResume() {
		super.onResume();
		SQLiteAdapter myDbHelper = new SQLiteAdapter(null);
		ArrayList<SearchResults> localEventResults = myDbHelper.getFriEvents();
		friEventResults.clear();
		friEventResults.addAll(localEventResults);
		adapter.notifyDataSetChanged();
	}
}