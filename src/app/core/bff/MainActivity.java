package app.core.bff;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;


public class MainActivity extends Activity implements OnClickListener{

	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE); 
        setContentView(R.layout.activity_main);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
        findViewById(R.id.openMenu).setOnClickListener(this);
        
    }
    
	@Override
	public void onResume() {
		super.onResume();
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    @Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.openMenu) {
			openMenuActivity();
		} else {
		}
	}
    
    private void openMenuActivity(){
		int width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
		SlideoutActivity.prepare(MainActivity.this, R.id.inner_content, width);
		startActivity(new Intent(MainActivity.this, MenuActivity.class));
		overridePendingTransition(0, 0);
    }
}
