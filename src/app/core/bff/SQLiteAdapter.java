package app.core.bff;


import java.io.File;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class SQLiteAdapter extends SQLiteOpenHelper {
	
	// Specifying database path and database name to use
	//OLD WAY private static String DB_PATH = "/data/data/app.core.wcf/databases/";
	private static String DB_PATH = "/data/data/app.core.bff/files/";
	private static String CURRENT_DB_NAME = "currentbff.sqlite";
	private static String NEXT_DB_NAME = "nextbff.sqlite";

	private static SQLiteDatabase myDB;
	private final Context myContext;

	SQLiteAdapter (Context context) {
		super(context, CURRENT_DB_NAME, null, 1);
		this.myContext=context;
 	}
	
	public void createDataBase() throws IOException{
		
		boolean dbExist = checkDB();

		if(dbExist){
			// Update the DB from network data if there is any
			updateDB();
		}
		else {

			try{
				copyDB();
				updateDB();
			}catch (IOException e) {
				throw new Error("Database could not be copied");
			}
		}
	}

 	private boolean checkDB(){
 		SQLiteDatabase checkDB = null;
 		try{
 			String myPath = DB_PATH + CURRENT_DB_NAME;
 			checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);		
 		}catch (SQLiteException e) {
 			
 		//database does not yet exist
 		}
 		if(checkDB != null){
 			checkDB.close();
 		}
 	
 		return checkDB != null ? true : false;
 	}
 	
 	private boolean isNetworkAvailable() {
 	    ConnectivityManager connectivityManager 
 	          = (ConnectivityManager) myContext.getSystemService(Context.CONNECTIVITY_SERVICE);
 	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
 	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
 	}

 	private void copyDB() throws IOException {

		//This is needed to create the database directory on the first run of the WCF Application
		File databaseDirectory = new File(myContext.getFilesDir().getPath());
		//open local as input stream
		InputStream myInputStream = myContext.getAssets().open(NEXT_DB_NAME);

		//path to newly created && empty db
		String outFileName = DB_PATH + CURRENT_DB_NAME;

		//open empty db as output stream
		OutputStream myOutputStream = new FileOutputStream(outFileName);

		//transfer bytes
 		byte[] buffer = new byte[1024];
 		int length;
 		while ((length = myInputStream.read(buffer))>0){
 			myOutputStream.write(buffer, 0, length);
 		}

 		myOutputStream.flush();
 		myOutputStream.close();
 		myInputStream.close();
 	}

 	public void openDB() throws SQLiteException{
    	 		
    	String myPath = DB_PATH + CURRENT_DB_NAME;
 		myDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
 	}
 
	public Long lastTimeUpdated() {
		SharedPreferences preferences = myContext.getSharedPreferences("BFFPreferences", Context.MODE_PRIVATE);
		// Return value (default of 1374619166284 (which represents about 12:30 on 7/23/2013
		// which is when we put the latest db into the app) if the preference doesn't exist
		return preferences.getLong("lastUpdateTime", 1374619166284L);
	}

	public void updateLastTimeUpdated() {

		Date now = new Date();
		Long timeStamp = now.getTime();
		SharedPreferences preferences = myContext.getSharedPreferences("BFFPreferences", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putLong("lastUpdateTime", timeStamp);
		editor.commit();
	}

	public void updateDB() {

		if (!isNetworkAvailable())
			return;

		JSONArray eventsTable = new JSONArray();
		JSONArray commercialTable = new JSONArray();
		JSONArray carnivalTable = new JSONArray();
		JSONArray locationsTable = new JSONArray();
		JSONArray foodTable = new JSONArray();
		String url = "http://bigfairfun.com/apps/update.php?lastTimeUpdated=";
		String EVENTS = "events";
		String FOOD = "food_vendors";
		String SHOPPING = "commercial_vendors";
		String RIDES = "carnival_rides";
		String LOCATIONS = "locations";

		try {
			url = url + lastTimeUpdated().toString();
		// Disabled the update code for now.  Hopefully will get it working.
		final boolean ENABLE_UPDATE = true;
		if (ENABLE_UPDATE) {
		JSONObject json = new JSONParser().execute(url).get();

		if (!json.isNull(EVENTS))
			eventsTable = json.getJSONArray(EVENTS);
		if (!json.isNull(FOOD))
			foodTable = json.getJSONArray(FOOD);
		if (!json.isNull(SHOPPING))
			commercialTable = json.getJSONArray(SHOPPING);
		if (!json.isNull(RIDES))
			carnivalTable = json.getJSONArray(RIDES);
		if (!json.isNull(LOCATIONS))
			locationsTable = json.getJSONArray(LOCATIONS);

		}
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ExecutionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			openDB();

			String tableName = "events";
			for (int i = 0; i < eventsTable.length(); ++i) {
				JSONObject rec = eventsTable.getJSONObject(i);
				Integer id = rec.getInt("_id");
				String sid = String.valueOf(id);
				String query = "SELECT _id FROM " + tableName + " WHERE _id = " + sid;
				Cursor c = myDB.rawQuery(query, null);
				boolean recordExists = (c != null && c.moveToFirst());

				if (1 == rec.getInt("is_deleted")) {
					if (recordExists) {
						myDB.delete(tableName, "_id = " + sid, null);
					}
				}
				else {
					// Add or modify a record
					ContentValues values = new ContentValues();
					values.put("date", rec.getString("date"));
					values.put("day", rec.getString("day"));
					values.put("start_time", rec.getString("start_time"));
					values.put("end_time", rec.getString("end_time"));
					values.put("event_description", rec.getString("event_description"));
					values.put("location", rec.getString("location"));
					values.put("category", rec.getString("category"));
					values.put("location_id", rec.getInt("location_id"));
					values.put("details", rec.getString("details"));
					values.put("image", rec.getString("image"));
					values.put("is_valid", rec.getInt("is_valid"));
					values.put("invalid_description", rec.getString("invalid_description"));
					values.put("is_deleted", rec.getInt("is_deleted"));
					values.put("_id", rec.getInt("_id"));

					if (recordExists) {
						myDB.update(tableName, values, "_id = " + sid, null);
					}
					else
					{
						values.put("user_favorite", 0);
						myDB.insert(tableName, null, values);
					};
				}
			}

			tableName = "food_vendors";
			for (int i = 0; i < foodTable.length(); ++i) {
				JSONObject rec = foodTable.getJSONObject(i);
				Integer id = rec.getInt("_id");
				String sid = String.valueOf(id);
				String query = "SELECT _id FROM " + tableName + " WHERE _id = " + sid;
				Cursor c = myDB.rawQuery(query, null);
				boolean recordExists = (c != null && c.moveToFirst());

				if (1 == rec.getInt("is_deleted")) {
					if (recordExists) {
						myDB.delete(tableName, "_id = " + sid, null);
					}
				}
				else {
					// Add or modify a record
					ContentValues values = new ContentValues();
					values.put("company", rec.getString("company"));
					values.put("booth_number", rec.getString("booth_number"));
					values.put("menu", rec.getString("menu"));
					values.put("booth_size", rec.getString("booth_size"));
					values.put("cuisine", rec.getString("cuisine"));
					values.put("location", rec.getString("location"));
					values.put("category", rec.getString("category"));
					values.put("location_id", rec.getInt("location_id"));
					values.put("details", rec.getString("details"));
					values.put("image", rec.getString("image"));
					values.put("is_valid", rec.getInt("is_valid"));
					values.put("invalid_description", rec.getString("invalid_description"));
					values.put("is_deleted", rec.getInt("is_deleted"));
					values.put("_id", rec.getInt("_id"));

					if (recordExists) {
						myDB.update(tableName, values, "_id = " + sid, null);
					}
					else
					{
						values.put("user_favorite", 0);
						myDB.insert(tableName, null, values);
					};
				}
			}

			tableName = "carnival_rides";
			for (int i = 0; i < carnivalTable.length(); ++i) {
				JSONObject rec = carnivalTable.getJSONObject(i);
				Integer id = rec.getInt("_id");
				String sid = String.valueOf(id);
				String query = "SELECT _id FROM " + tableName + " WHERE _id = " + sid;
				Cursor c = myDB.rawQuery(query, null);
				boolean recordExists = (c != null && c.moveToFirst());

				if (1 == rec.getInt("is_deleted")) {
					if (recordExists) {
						myDB.delete(tableName, "_id = " + sid, null);
					}
				}
				else {
					// Add or modify a record
					ContentValues values = new ContentValues();
					values.put("ride_name", rec.getString("ride_name"));
					values.put("coupons", rec.getInt("coupons"));
					values.put("ride_alone_height", rec.getString("ride_alone_height"));
					values.put("with_adult_height", rec.getString("with_adult_height"));
					values.put("special_rules", rec.getString("special_rules"));
					values.put("description", rec.getString("description"));
					values.put("category", rec.getString("category"));
					values.put("location_id", rec.getInt("location_id"));
					values.put("details", rec.getString("details"));
					values.put("image", rec.getString("image"));
					values.put("is_valid", rec.getInt("is_valid"));
					values.put("invalid_description", rec.getString("invalid_description"));
					values.put("is_deleted", rec.getInt("is_deleted"));
					values.put("_id", rec.getInt("_id"));

					if (recordExists) {
						myDB.update(tableName, values, "_id = " + sid, null);
					}
					else
					{
						values.put("user_favorite", 0);
						myDB.insert(tableName, null, values);
					};
				}
			}

			tableName = "commercial_vendors";
			for (int i = 0; i < commercialTable.length(); ++i) {
				JSONObject rec = commercialTable.getJSONObject(i);
				Integer id = rec.getInt("_id");
				String sid = String.valueOf(id);
				String query = "SELECT _id FROM " + tableName + " WHERE _id = " + sid;
				Cursor c = myDB.rawQuery(query, null);
				boolean recordExists = (c != null && c.moveToFirst());

				if (1 == rec.getInt("is_deleted")) {
					if (recordExists) {
						myDB.delete(tableName, "_id = " + sid, null);
					}
				}
				else {
					// Add or modify a record
					ContentValues values = new ContentValues();
					values.put("business_name", rec.getString("business_name"));
					values.put("products", rec.getString("products"));
					values.put("space_number", rec.getString("space_number"));
					values.put("type", rec.getString("type"));
					values.put("location", rec.getString("location"));
					values.put("category", rec.getString("category"));
					values.put("location_id", rec.getInt("location_id"));
					values.put("details", rec.getString("details"));
					values.put("image", rec.getString("image"));
					values.put("is_valid", rec.getInt("is_valid"));
					values.put("invalid_description", rec.getString("invalid_description"));
					values.put("is_deleted", rec.getInt("is_deleted"));
					values.put("_id", rec.getInt("_id"));

					if (recordExists) {
						myDB.update(tableName, values, "_id = " + sid, null);
					}
					else
					{
						values.put("user_favorite", 0);
						myDB.insert(tableName, null, values);
					};
				}
			}

			tableName = "locations";
			for (int i = 0; i < locationsTable.length(); ++i) {
				JSONObject rec = locationsTable.getJSONObject(i);
				Integer id = rec.getInt("_id");
				String sid = String.valueOf(id);
				String query = "SELECT _id FROM " + tableName + " WHERE _id = " + sid;
				Cursor c = myDB.rawQuery(query, null);
				boolean recordExists = (c != null && c.moveToFirst());

				if (1 == rec.getInt("is_deleted")) {
					if (recordExists) {
						myDB.delete(tableName, "_id = " + sid, null);
					}
				}
				else {
					// Add or modify a record
					ContentValues values = new ContentValues();
					values.put("location_description", rec.getString("location_description"));
					values.put("category", rec.getString("category"));
					values.put("lat", rec.getDouble("lat"));
					values.put("lon", rec.getDouble("lon"));
					values.put("image", rec.getString("image"));
					values.put("is_valid", rec.getInt("is_valid"));
					values.put("invalid_description", rec.getString("invalid_description"));
					values.put("is_deleted", rec.getInt("is_deleted"));
					values.put("_id", rec.getInt("_id"));

					if (recordExists) {
						myDB.update(tableName, values, "_id = " + sid, null);
					}
					else
					{
						myDB.insert(tableName, null, values);
					};
				}
			}

		} catch (SQLException sqle) {
			throw sqle;
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
		finally {
			updateLastTimeUpdated();
			close();
		}

	}
	public String convert24HourTo12(String time24)
	{
		String[] startTimeArray = time24.split(":");
		String sStartHour = startTimeArray[0];//cut from, for example, "12:30" to "12"
		String sStartMinute = startTimeArray[1];
		int start = Integer.parseInt(sStartHour);
		String adjustedStart = null;
		if(start < 13)
		{
			if(start == 12)
			{
				adjustedStart = String.valueOf(start) + ":" + sStartMinute + " PM";
			}
			else{
			adjustedStart = String.valueOf(start) + ":" + sStartMinute + " AM";
			}
		}
		else{
			start = start - 12;
			adjustedStart = String.valueOf(start) + ":" + sStartMinute + " PM";
		}
		
		return adjustedStart;
}
 	
	public ArrayList<SearchResults> getEventsWithCondition(String date,
			String condition,
			boolean favoritesOnly) {
	ArrayList<SearchResults> results = new ArrayList<SearchResults>();

	try {
		openDB();

		String[] searchArgs = null;
		String query = "SELECT * FROM events";
		String joiner = " WHERE ";
		if (date != null) {
			query += " WHERE date = '" + date + "'";
			joiner = " AND ";
		}
		if (favoritesOnly) {
			query += joiner + "user_favorite = '1'";
		}
		if (condition != null) {
			searchArgs = new String[7];
			Arrays.fill(searchArgs, condition);
			query += joiner + "event_description LIKE ?" +
					" OR start_time LIKE ?" +
					" OR end_time LIKE ?" +
					" OR day LIKE ?" +
					" OR location LIKE ?" +
					" OR category LIKE ?" +
					" OR details LIKE ?";
		}

		Cursor c = myDB.rawQuery(query, searchArgs);
		if (c != null) {
			if (c.moveToFirst()) {
				do {
					SearchResults sr1 = new SearchResults();
					sr1.setType("event");
					sr1.setId(c.getInt(c.getColumnIndex("_id")));
					sr1.setCheckBox(c.getInt(c.getColumnIndex("user_favorite")));
					sr1.setImage(c.getString(c.getColumnIndex("image")));
					sr1.setColumn1(c.getString(c.getColumnIndex("event_description")));
					sr1.setColumn2(c.getString(c.getColumnIndex("day")));
					String startTime = convert24HourTo12(c.getString(c.getColumnIndex("start_time")));
					sr1.setColumn3(startTime);
					sr1.setColumn4(c.getString(c.getColumnIndex("location")));
					String endTime = convert24HourTo12(c.getString(c.getColumnIndex("end_time")));
					sr1.setColumn5(endTime);
					sr1.setIsValid(c.getInt(c.getColumnIndex("is_valid")) == 1);
					String invalidReason = c.getString(c.getColumnIndex("invalid_description"));
					if (null == invalidReason) invalidReason = "Cancelled";
					sr1.setInvalidReason(invalidReason);
					// Replace placeholders in details string
					String category = c.getString(c.getColumnIndex("category"));
					String eventDate = c.getString(c.getColumnIndex("date"));
					String details = c.getString(c.getColumnIndex("details"));
					// Make sure none of the strings are null
					if (null == category) category = "";
					if (null == eventDate) eventDate = "";
					details = details.replace("__event_description__", sr1.getColumn1());
					details = details.replace("__date__", eventDate);
					details = details.replace("__day__",  sr1.getColumn2());
					details = details.replace("__start_time__",  sr1.getColumn3());
					details = details.replace("__end_time__", sr1.getColumn5());
					details = details.replace("__location__", sr1.getColumn4());
					details = details.replace("__category__",  category);
					sr1.setDetails(details);
					// For now don't include invalid items
					if (sr1.getIsValid()) {
						results.add(sr1);
					}

				} while (c.moveToNext());
			}
		}
		close();

	} catch (SQLException sqle) {
		throw sqle;
	}
	return results;
}

public ArrayList<SearchResults> getAllDayEvents() {
	return getAllDayEventsWithCondition(null, false);
}

public ArrayList<SearchResults> getAllDayEventsWithCondition(String condition,
			boolean favoritesOnly) {
	return getEventsWithCondition(null, condition, favoritesOnly);
}

public ArrayList<SearchResults> getThursEvents() {
	return getThursEventsWithCondition(null, false);
}

public ArrayList<SearchResults> getThursEventsWithCondition(String condition,
		boolean favoritesOnly) {
	return getEventsWithCondition("7/25/13", condition, favoritesOnly);
}

public ArrayList<SearchResults> getFriEvents() {
	return getFriEventsWithCondition(null, false);
}

public ArrayList<SearchResults> getFriEventsWithCondition(String condition,
			boolean favoritesOnly) {
	return getEventsWithCondition("7/26/13", condition, favoritesOnly);
}

public ArrayList<SearchResults> getSatEvents() {
	return getSatEventsWithCondition(null, false);
}

public ArrayList<SearchResults> getSatEventsWithCondition(String condition,
		boolean favoritesOnly) {
	return getEventsWithCondition("7/27/13", condition, favoritesOnly);
}

public ArrayList<SearchResults> getSunEvents() {
	return getSunEventsWithCondition(null, false);
}

public ArrayList<SearchResults> getSunEventsWithCondition(String condition,
		boolean favoritesOnly) {
	return getEventsWithCondition("7/28/13", condition, favoritesOnly);
}

public ArrayList<SearchResults> sortCarnivalResults(
		ArrayList<SearchResults> results) {
	// The DB sort order produces "Hair Raising", followed by
	// "Kid Friendly", "Mildly Spirited",
	// and "Thrill Seeker". This is almost the correct order from least to
	// most intense, we just
	// need to move the "Hair Raising" items to the bottom.
	ArrayList<SearchResults> newResults = new ArrayList<SearchResults>(
			results);
	for (SearchResults sr : results) {
		if (sr.getColumn4().equals("Hair Raising")) {
			newResults.remove(sr);
			newResults.add(sr);
		}
	}
	return newResults;
}

public ArrayList<SearchResults> getCarnivalWithCondition(String condition,
		boolean favoritesOnly) {
	ArrayList<SearchResults> fixedResults = new ArrayList<SearchResults>();
	try {
		openDB();

		String[] searchArgs = null;
		String joiner = " WHERE ";
		String query = "SELECT * FROM carnival_rides";
		if (condition != null) {
			searchArgs = new String[7];
			Arrays.fill(searchArgs, condition);
			query += " WHERE ride_name LIKE ?" +
					" OR category LIKE ?" +
					" OR description LIKE ?" +
					" OR details LIKE ?" +
					" OR ride_alone_height LIKE ?" +
					" OR with_adult_height LIKE ?" +
					" OR special_rules LIKE ?";
			joiner = " AND ";
		}
		if (favoritesOnly) {
			query += joiner + "user_favorite = '1'";
		}
		query +=  " ORDER BY category, ride_name";


		ArrayList<SearchResults> results = new ArrayList<SearchResults>();
		Cursor c = myDB.rawQuery(query, searchArgs);
		if (c != null) {
			if (c.moveToFirst()) {
				do {
					SearchResults src = new SearchResults();
					src.setType("carnival");
					src.setId(c.getInt(c.getColumnIndex("_id")));
					src.setCheckBox(c.getInt(c.getColumnIndex("user_favorite")));
					src.setImage(c.getString(c.getColumnIndex("image")));
					src.setColumn1(c.getString(c.getColumnIndex("ride_name")));
					src.setColumn2(c.getString(c.getColumnIndex("coupons")));
					src.setColumn3(c.getString(c.getColumnIndex("ride_alone_height")));
					src.setColumn4(c.getString(c.getColumnIndex("category")));
					src.setIsValid(c.getInt(c.getColumnIndex("is_valid")) == 1);
					String invalidReason = c.getString(c.getColumnIndex("invalid_description"));
					if (null == invalidReason) invalidReason = "Not Available";
					src.setInvalidReason(invalidReason);
					// Replace placeholders in details string
					String description = c.getString(c.getColumnIndex("description"));
					String coupons = c.getString(c.getColumnIndex("coupons"));
					String withAdultHeight = c.getString(c.getColumnIndex("with_adult_height"));
					String specialRules = c.getString(c.getColumnIndex("special_rules"));
					String details = c.getString(c.getColumnIndex("details"));
					// Make sure none of the strings are null
					if (null == description) description = "";
					if (null == coupons) coupons = "";
					if (null == withAdultHeight) withAdultHeight = "";
					if (null == specialRules) specialRules = "";
					details = details.replace("__ride_name__", src.getColumn1());
					details = details.replace("__description__", description);
					details = details.replace("__coupons__",  coupons);
					details = details.replace("__ride_alone_height__",  src.getColumn3());
					details = details.replace("__with_adult_height__", withAdultHeight);
					details = details.replace("__special_rules__", specialRules);
					details = details.replace("__category__",  src.getColumn4());
					src.setDetails(details);
					// For now don't include invalid items
					if (src.getIsValid()) {
						results.add(src);
					}

				} while (c.moveToNext());
			}
		}
		fixedResults = sortCarnivalResults(results);
		close();

	} catch (SQLException sqle) {
		throw sqle;
	}
	return fixedResults;
}

public ArrayList<SearchResults> getCarnival() {
	return getCarnivalWithCondition(null, false);
}

public ArrayList<SearchResults> getFoodWithCondition(String condition,
		boolean favoritesOnly) {
	ArrayList<SearchResults> results = new ArrayList<SearchResults>();
	try {
		openDB();

		String[] searchArgs = null;
		String joiner = " WHERE ";
		String query = "SELECT * FROM food_vendors";
		if (condition != null) {
			searchArgs = new String[5];
			Arrays.fill(searchArgs, condition);
			query += joiner + "company LIKE ?" +
					" OR menu LIKE ?" +
					" OR cuisine LIKE ?" +
					" OR location LIKE ?" +
					" OR details LIKE ?";
			joiner = " AND ";
		}
		if (favoritesOnly) {
			query += joiner + "user_favorite = '1'";
		}
		query +=  " ORDER BY cuisine, company";

		Cursor c = myDB.rawQuery(query, searchArgs);
		if (c != null) {
			if (c.moveToFirst()) {
				do {
					SearchResults src = new SearchResults();
					src.setType("food");
					src.setId(c.getInt(c.getColumnIndex("_id")));
					src.setCheckBox(c.getInt(c.getColumnIndex("user_favorite")));
					src.setImage(c.getString(c.getColumnIndex("image")));
					src.setColumn1(c.getString(c.getColumnIndex("company")));
					src.setColumn2(c.getString(c.getColumnIndex("booth_number")));
					src.setColumn3(c.getString(c.getColumnIndex("location")));
					src.setColumn4(c.getString(c.getColumnIndex("cuisine")));
					src.setIsValid(c.getInt(c.getColumnIndex("is_valid")) == 1);
					String invalidReason = c.getString(c.getColumnIndex("invalid_description"));
					if (null == invalidReason) invalidReason = "Not Available";
					src.setInvalidReason(invalidReason);
					// Replace placeholders in details string
					String address = c.getString(c.getColumnIndex("address"));
					String phone = c.getString(c.getColumnIndex("phone"));
					String cityStateZip = c.getString(c.getColumnIndex("city_state_zip"));
					String email = c.getString(c.getColumnIndex("email"));
					String menu = c.getString(c.getColumnIndex("menu"));
					String owner = c.getString(c.getColumnIndex("owner"));
					String details = c.getString(c.getColumnIndex("details"));
					// Make sure none of the strings are null
					if (null == address) address = "";
					if (null == phone) phone = "";
					if (null == cityStateZip) cityStateZip = "";
					if (null == email) email = "";
					if (null == menu) menu = "";
					if (null == owner) owner = "";
					details = details.replace("__company__", src.getColumn1());
					details = details.replace("__city_state_zip__", cityStateZip);
					details = details.replace("__address__",  address);
					details = details.replace("__phone__",  phone);
					details = details.replace("__email__", email);
					details = details.replace("__menu__", menu);
					details = details.replace("__owner__",  owner);
					details = details.replace("__location__",  src.getColumn3());
					details = details.replace("__cuisine__",  src.getColumn4());
					src.setDetails(details);
					// For now don't include invalid items
					if (src.getIsValid()) {
						results.add(src);
					}

				} while (c.moveToNext());
			}
		}
		close();

	} catch (SQLException sqle) {
		throw sqle;
	}
	return results;
}

public ArrayList<SearchResults> getFood() {
	return getFoodWithCondition(null, false);
}

// the following is a big part of code for populating the favorite array,
// and has 4 parts
public ArrayList<SearchResults> getFavorite() {
	ArrayList<SearchResults> results = new ArrayList<SearchResults>();
	results = getFoodWithCondition(null, true);
	results.addAll(getAllDayEventsWithCondition(null, true));
	results.addAll(getShoppingWithCondition(null, true));
	results.addAll(getCarnivalWithCondition(null, true));
	return results;
}

public ArrayList<SearchResults> getShoppingWithCondition(String condition,
		boolean favoritesOnly) {
	ArrayList<SearchResults> results = new ArrayList<SearchResults>();
	try {
		openDB();

		String[] searchArgs = null;
		String joiner = " WHERE ";
		String query = "SELECT * FROM commercial_vendors";
		if (condition != null) {
			searchArgs = new String[6];
			Arrays.fill(searchArgs, condition);
			query += joiner + "business_name LIKE ?" +
					" OR products LIKE ?" +
					" OR type LIKE ?" +
					" OR location LIKE ?" +
					" OR category LIKE ?" +
					" OR details LIKE ?";
			joiner = " AND ";
		}
		if (favoritesOnly) {
			query += joiner + "user_favorite = '1'";
		}
		query +=  " ORDER BY type, business_name";

		Cursor c = myDB.rawQuery(query, searchArgs);
		if (c != null) {
			if (c.moveToFirst()) {
				do {
					SearchResults src = new SearchResults();
					src.setType("commercial");
					src.setId(c.getInt(c.getColumnIndex("_id")));
					src.setCheckBox(c.getInt(c.getColumnIndex("user_favorite")));
					src.setImage(c.getString(c.getColumnIndex("image")));
					src.setColumn1(c.getString(c.getColumnIndex("business_name")));
					src.setColumn2(c.getString(c.getColumnIndex("products")));
					src.setColumn3(c.getString(c.getColumnIndex("type")));
					src.setColumn4(c.getString(c.getColumnIndex("location")));
					src.setIsValid(c.getInt(c.getColumnIndex("is_valid")) == 1);
					String invalidReason = c.getString(c.getColumnIndex("invalid_description"));
					if (null == invalidReason) invalidReason = "Not Available";
					src.setInvalidReason(invalidReason);
					// Replace placeholders in details string
					String address = c.getString(c.getColumnIndex("address"));
					String phone = c.getString(c.getColumnIndex("phone1"));
					String cityStateZip = c.getString(c.getColumnIndex("city_state_zip"));
					String email = c.getString(c.getColumnIndex("email"));
					String owner = c.getString(c.getColumnIndex("owner"));
					String category = c.getString(c.getColumnIndex("category"));
					String details = c.getString(c.getColumnIndex("details"));
					// Make sure none of the strings are null
					if (null == address) address = "";
					if (null == phone) phone = "";
					if (null == cityStateZip) cityStateZip = "";
					if (null == email) email = "";
					if (null == owner) owner = "";
					if (null == category) category = "";
					details = details.replace("__business_name__", src.getColumn1());
					details = details.replace("__city_state_zip__", cityStateZip);
					details = details.replace("__address__",  address);
					details = details.replace("__phone1__",  phone);
					details = details.replace("__email__", email);
					details = details.replace("__products__", src.getColumn2());
					details = details.replace("__owner__",  owner);
					details = details.replace("__type__",  src.getColumn3());
					details = details.replace("__location__",  src.getColumn4());
					details = details.replace("__category__",  category);
					src.setDetails(details);
					// For now don't include invalid items
					if (src.getIsValid()) {
						results.add(src);
					}

				} while (c.moveToNext());
			}
		}
		close();

	} catch (SQLException sqle) {
		throw sqle;
	}
	return results;
}


public ArrayList<SearchResults> getShopping() {
	return getShoppingWithCondition(null, false);
}

public boolean isFavorite(String type, Integer id) {
	boolean retVal = false;
	try {
		openDB();

		// Choose the correct table to query
		String table = "";
		if (type == "event") table = "events";
		else if (type == "carnival") table = "carnival_rides";
		else if (type == "food") table = "food_vendors";
		else if (type == "commercial") table = "commercial_vendors";
		String query = "SELECT user_favorite FROM " + table + " WHERE _id = '"
					+ Integer.toString(id) + "'";

		Cursor c = myDB.rawQuery(query, null);
		if (c != null) {
			if (c.moveToFirst()) {
				do {
					Integer isFavorite = c.getInt(c.getColumnIndex("user_favorite"));
					if (isFavorite == 1) {
						retVal = true;
					}
				} while (c.moveToNext());
			}
		}
		close();

	} catch (SQLException sqle) {
		throw sqle;
	}
	return retVal;
}

public void setIsFavorite(String type, Integer id, boolean value) {
	try {
		openDB();

		// Choose the correct table to update
		String table = "";
		if (type == "event") table = "events";
		else if (type == "carnival") table = "carnival_rides";
		else if (type == "food") table = "food_vendors";
		else if (type == "commercial") table = "commercial_vendors";

		String favoriteValue = (value ? "1" : "0");

		ContentValues values = new ContentValues();
		values.put("user_favorite", favoriteValue);
		String[] whereArgs = new String[] {String.valueOf(id)};
		myDB.update(table, values, "_id=?", whereArgs);

		close();

	} catch (SQLException sqle) {
		throw sqle;
	}
}
 	
 // map : get locations
 	
 	
 	 
public ArrayList<SearchResultsMap> getlocations() {
 		
 		ArrayList<SearchResultsMap> results = new ArrayList<SearchResultsMap>();
 		
 		Cursor c = myDB.rawQuery("SELECT * FROM locations", null);
 		if (c != null ) {	
 			if (c.moveToFirst()) {
 				do {
 					SearchResultsMap src = new SearchResultsMap();
 			    	src.setLat(c.getDouble(c.getColumnIndex("lat")));
 			    	src.setLon(c.getDouble(c.getColumnIndex("lon")));
 			    	src.setLocationDescription(c.getString(c.getColumnIndex("location_description")));
 			    	src.setCat(c.getString(c.getColumnIndex("category")));
 			    	results.add(src);
 	    			
 				} while (c.moveToNext());
 			}
 		}
 		return results;
 	}	
 	
 	
 	
	@Override
 	public synchronized void close(){
 			if(myDB != null)
 				myDB.close();
 			super.close();
 	}

 	@Override
 	public void onCreate(SQLiteDatabase db) {
 		// TODO Auto-generated method stub
 	
 	}

 	@Override
 	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
 		// TODO Auto-generated method stub
 			
 	}
 		
}