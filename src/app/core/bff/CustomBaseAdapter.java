package app.core.bff;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomBaseAdapter extends BaseAdapter {
        private static ArrayList<SearchResults> scheduleArrayList;
        
        private LayoutInflater mInflater;
        private Context myContext;
        
        public CustomBaseAdapter(Context context, ArrayList<SearchResults> results) {
                scheduleArrayList = results;
                mInflater = LayoutInflater.from(context);
                myContext = context;
        }
        
        public int getCount() {
                return scheduleArrayList.size();
        }

        public Object getItem(int position) {
                return scheduleArrayList.get(position);
        }

        public long getItemId(int position) {
                return position;
        }

        public View getView( final int position, View convertView, ViewGroup parent) {
                ViewHolder holder;
                if (convertView == null) {
                        convertView = mInflater.inflate(R.layout.schedule_layout, null);
                        holder = new ViewHolder();
                        holder.checkBox = (CheckBox) convertView.findViewById(R.id.favbutton);
                        holder.image = (ImageView) convertView.findViewById(R.id.icon);
                        holder.column1 = (TextView) convertView.findViewById(R.id.column1);
                        holder.column2 = (TextView) convertView.findViewById(R.id.column2);
                        holder.column3 = (TextView) convertView.findViewById(R.id.column3);

                        convertView.setTag(holder);
                } else {
                        holder = (ViewHolder) convertView.getTag();
                }
                
                String temp = scheduleArrayList.get(position).getImage();
        		temp = temp.substring(0, temp.length()-4);//getIdentifier only uses the base file name, not the '.png' suffix
        		int resId = myContext.getResources().getIdentifier(temp, "drawable", "app.core.bff");//this code should be checked
        		holder.image.setImageResource(resId);
    			String dayAndTime = scheduleArrayList.get(position).getColumn2() + " from "
    					+ scheduleArrayList.get(position).getColumn3() + " to "
    					+ scheduleArrayList.get(position).getColumn5();
    			holder.column1.setText(scheduleArrayList.get(position).getColumn1());
                holder.column2.setText(dayAndTime);
                holder.column3.setText(scheduleArrayList.get(position).getColumn4());


                holder.checkBox.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                			Integer id = scheduleArrayList.get(position).getId();
                			SQLiteAdapter myDbHelper = new SQLiteAdapter(myContext);
                			myDbHelper.setIsFavorite("event", id, ((CheckBox)v).isChecked());
                        }
                });

                SQLiteAdapter myDbHelper = new SQLiteAdapter(myContext);
                Integer id = scheduleArrayList.get(position).getId();
                holder.checkBox.setChecked(myDbHelper.isFavorite("event", id));
                
                return convertView;
        }

        static class ViewHolder {
                TextView column1;
                TextView column2;
                TextView column3;
                CheckBox checkBox;
                ImageView image;
        }
}