package app.core.bff;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class FavoritesActivity extends Activity implements OnClickListener{
static private ArrayList<SearchResults> favoriteResults = new ArrayList<SearchResults>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_favorites);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
        
        TextView actTv = (TextView) findViewById(R.id.titleHeading);
        actTv.setText(R.string.title_activity_favorites);
        findViewById(R.id.openMenu).setOnClickListener(this);
        findViewById(R.id.openSearch).setOnClickListener(this);

        ListView lv1 = (ListView) findViewById(R.id.favoriteList);
        lv1.setAdapter(new CustomAdapterFavorite(this, favoriteResults));

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent detailIntent = new Intent(FavoritesActivity.this, DetailViewActivity.class);

                CustomAdapterFavorite ca = (CustomAdapterFavorite)parent.getAdapter();
                SearchResults sr = (SearchResults)ca.getItem(position);

                detailIntent.putExtra("details", sr.getDetails());
                startActivity(detailIntent);
            }
        });
    }
    
    @Override
    public void onResume() {
        super.onResume();
        SQLiteAdapter myDbHelper = new SQLiteAdapter(this);
        ArrayList<SearchResults> localFavoriteResults = myDbHelper.getFavorite();
        favoriteResults.clear();
        favoriteResults.addAll(localFavoriteResults);
        ListView lv = (ListView) findViewById(R.id.favoriteList);
        BaseAdapter adapter = (BaseAdapter)lv.getAdapter();
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    @Override
        public void onClick(View v) {
                int id = v.getId();
				if (id == R.id.openMenu) {
					openMenuActivity();
				} else if (id == R.id.openSearch) {
					startActivity(new Intent(FavoritesActivity.this, SearchActivity.class));//open search activity, THEN open search dialog
				} else {
				}
        }
    
    private void openMenuActivity(){
                int width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
                SlideoutActivity.prepare(FavoritesActivity.this, R.id.inner_content, width);
                startActivity(new Intent(FavoritesActivity.this, MenuActivity.class));
                overridePendingTransition(0, 0);
    }
   
        
}