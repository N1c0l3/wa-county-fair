package app.core.bff;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomAdapterFood extends BaseAdapter {
	private static ArrayList<SearchResults> foodArrayList;
	
	private LayoutInflater mInflater;
    private Context myContext;

	public CustomAdapterFood(Context context, ArrayList<SearchResults> results) {
		foodArrayList = results;
		mInflater = LayoutInflater.from(context);
        myContext = context;
	}
	
	public int getCount() {
		return foodArrayList.size();
	}

	public Object getItem(int position) {
		return foodArrayList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.food_layout, null);
			holder = new ViewHolder();
			holder.checkBox = (CheckBox) convertView.findViewById(R.id.favbutton);
			holder.image = (ImageView) convertView.findViewById(R.id.icon);
			holder.column1 = (TextView) convertView.findViewById(R.id.column1);
			holder.column2 = (TextView) convertView.findViewById(R.id.column2);
			holder.column3 = (TextView) convertView.findViewById(R.id.column3);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		String temp = foodArrayList.get(position).getImage();
		temp = temp.substring(0, temp.length()-4);//getIdentifier only uses the base file name, not the '.png' suffix
		int resId = myContext.getResources().getIdentifier(temp, "drawable", "app.core.bff");//this code should be checked
		holder.image.setImageResource(resId);
		holder.column1.setText(foodArrayList.get(position).getColumn1());
		holder.column2.setText(foodArrayList.get(position).getColumn4());
		holder.column3.setText(foodArrayList.get(position).getColumn3());

		SQLiteAdapter myDbHelper = new SQLiteAdapter(myContext);
		Integer id = foodArrayList.get(position).getId();
		holder.checkBox.setChecked(myDbHelper.isFavorite("food", id));

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View v) {
        		SQLiteAdapter myDbHelper = new SQLiteAdapter(myContext);
        		Integer id = foodArrayList.get(position).getId();
        		myDbHelper.setIsFavorite("food", id, ((CheckBox)v).isChecked());
        	}
        }); 

        return convertView;
	}

	static class ViewHolder {
		TextView column1;
		TextView column2;
		TextView column3;
		CheckBox checkBox;
		ImageView image;
	}
}