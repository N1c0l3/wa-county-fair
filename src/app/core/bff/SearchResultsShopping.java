package app.core.bff;

public class SearchResultsShopping {

	private String business_name = "";
	private String products = "";
	private String type = "";
	private String location = "";
	
	public void setBusinessName(String business_name) {
		this.business_name = business_name;
	}

	public String getBusinessName() {
		return business_name ;
	}

	public void setProducts(String products) {
		this.products = products;
	}

	public String getProducts() {
		return products;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}

	public String getLocation() {
		return location;
	}
	
}
