package app.core.bff;

public class SearchResults {
	
	private Integer id=0;
	private String type="";
	private String image="";
	private boolean isValid;
	private String invalidReason;
	private Integer checkBox=0;
	private String column1 = "";
	private String column2 = "";
	private String column3 = "";
	private String column4 = "";
	private String column5 = "";
	private String details = "";
	
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
	
	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public boolean getIsValid() {
		return isValid;
	}
	
	public void setInvalidReason(String invalidReason) {
		this.invalidReason = invalidReason;
	}

	public String getInvalidReason() {
		return invalidReason;
	}
	
	public void setCheckBox(Integer checkBox) {
		this.checkBox = checkBox;
	}

	public Integer getCheckBox() {
		return checkBox;
	}
	
	public void setImage(String image) {
		this.image = image;
	}

	public String getImage() {
		return image;
	}
	
	public void setColumn1(String column1) {
		this.column1 = column1;
	}

	public String getColumn1() {
		return column1;
	}

	public void setColumn2(String column2) {
		this.column2 = column2;
	}

	public String getColumn2() {
		return column2;
	}
	
	public void setColumn3(String column3) {
		this.column3 = column3;
	}

	public String getColumn3() {
		return column3;
	}
	
	public void setColumn4(String column4) {
		this.column4 = column4;
	}

	public String getColumn4() {
		return column4;
	}
	
	public void setColumn5(String column5) {
		this.column5 = column5;
	}

	public String getColumn5() {
		return column5;
	}
	
	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

}
