package app.core.bff;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.webkit.WebView;

public class DetailViewActivity extends Activity implements OnClickListener{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.detail_view);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
        findViewById(R.id.openMenu).setOnClickListener(this);
        findViewById(R.id.openSearch).setOnClickListener(this);
        
        Bundle bundle = this.getIntent().getExtras();
        WebView webView = (WebView)findViewById(R.id.detailView);
        webView.loadData(bundle.getString("details"), "text/html",  null);
    }

    @Override
	public void onResume() {
		super.onResume();
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

@Override
public void onClick(View v) {
	switch(v.getId()){
	case R.id.openMenu:
		openMenuActivity();
		break;
	case R.id.openSearch:
		startActivity(new Intent(DetailViewActivity.this, SearchActivity.class));//open search activity, THEN open search dialog
		break;
	default:
		break;
	}
}

private void openMenuActivity(){
	int width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
	SlideoutActivity.prepare(DetailViewActivity.this, R.id.inner_content, width);
	startActivity(new Intent(DetailViewActivity.this, MenuActivity.class));
	overridePendingTransition(0, 0);
	}
}
