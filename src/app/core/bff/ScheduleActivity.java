package app.core.bff;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.TabHost;
import android.widget.TextView;

public class ScheduleActivity extends FragmentActivity implements OnClickListener {
    
	// Tab identifiers 
	static String TAB_A = "All";
	static String TAB_B = "Thur";
	static String TAB_C = "Fri";
	static String TAB_D = "Sat";
	static String TAB_E = "Sun";
	
	TabHost mTabHost;
    
	AllFragment fragment1;
	ThurFragment fragment2;
	FriFragment fragment3;
	SatFragment fragment4;
	SunFragment fragment5;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_schedule);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
        
        TextView actTv = (TextView) findViewById(R.id.titleHeading);
        actTv.setText(R.string.title_activity_schedule);
		findViewById(R.id.openMenu).setOnClickListener(this);
		findViewById(R.id.openSearch).setOnClickListener(this);
//		File fos = new File("/data/data/app.core.wcf/databases/");
//		fos.mkdirs();
		        
    	fragment1 = new AllFragment();
    	fragment2 = new ThurFragment();
    	fragment3 = new FriFragment();
    	fragment4 = new SatFragment();
    	fragment5 = new SunFragment();
        
    	mTabHost = (TabHost)findViewById(android.R.id.tabhost);
    	mTabHost.setOnTabChangedListener(listener);
    	mTabHost.setup();
        
    	initializeTab();
    }
    
    @Override
	public void onResume() {
		super.onResume();
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
    	getMenuInflater().inflate(R.menu.activity_main, menu);
    	return true;
    }
    
    @Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.openMenu) {
			openMenuActivity();
		} else if (id == R.id.openSearch) {
			startActivity(new Intent(ScheduleActivity.this, SearchActivity.class));//open search activity, THEN open search dialog
		} else {
		}
	}
    
    private void openMenuActivity(){
		int width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
		SlideoutActivity.prepare(ScheduleActivity.this, R.id.inner_content, width);
		startActivity(new Intent(ScheduleActivity.this, MenuActivity.class));
		overridePendingTransition(0, 0);
    }  

     // Initialize the tabs and set views and identifiers for the tabs
    public void initializeTab() {
        
    	TabHost.TabSpec spec    =   mTabHost.newTabSpec(TAB_A);
        mTabHost.setCurrentTab(-3);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(android.R.id.tabcontent);
            }
        });
        spec.setIndicator(createTabView(TAB_A, R.drawable.ic_launcher));
        mTabHost.addTab(spec);


        spec                    =   mTabHost.newTabSpec(TAB_B);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(android.R.id.tabcontent);
            }
        });
        spec.setIndicator(createTabView(TAB_B, R.drawable.ic_launcher));
        mTabHost.addTab(spec);
        
        spec                    =   mTabHost.newTabSpec(TAB_C);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(android.R.id.tabcontent);
            }
        });
        spec.setIndicator(createTabView(TAB_C, R.drawable.ic_launcher));
        mTabHost.addTab(spec);
        
        spec                    =   mTabHost.newTabSpec(TAB_D);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(android.R.id.tabcontent);
            }
        });
        spec.setIndicator(createTabView(TAB_D, R.drawable.ic_launcher));
        mTabHost.addTab(spec);
        
        spec                    =   mTabHost.newTabSpec(TAB_E);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(android.R.id.tabcontent);
            }
        });
        spec.setIndicator(createTabView(TAB_E, R.drawable.ic_launcher));
        mTabHost.addTab(spec);
    }
    
     // TabChangeListener for changing the tab when one of the tabs is pressed
    TabHost.OnTabChangeListener listener    =   new TabHost.OnTabChangeListener() {
          public void onTabChanged(String tabId) {
            /*Set current tab..*/
              if(tabId.equals(TAB_A)){
                pushFragments(tabId, fragment1);
              }else if(tabId.equals(TAB_B)){
                pushFragments(tabId, fragment2);
              }else if(tabId.equals(TAB_C)){
                  pushFragments(tabId, fragment3);
              }else if(tabId.equals(TAB_D)){
                  pushFragments(tabId, fragment4);
              }else if(tabId.equals(TAB_E)){
                  pushFragments(tabId, fragment5);
              }
          }
    };
	        
    // Adds the fragment to the FrameLayout
    public void pushFragments(String tag, Fragment fragment){
        
        FragmentManager   manager         =   getSupportFragmentManager();
        FragmentTransaction ft            =   manager.beginTransaction();
        
        ft.replace(android.R.id.tabcontent, fragment);
        ft.commit();
    }
    
    // Returns the tab view i.e. the tab icon and text
     
    private View createTabView(final String text, final int id) {
        View view = LayoutInflater.from(this).inflate(R.layout.tabs_icon, null);
        ((TextView) view.findViewById(R.id.tab_text)).setText(text);
        return view;
    }
}