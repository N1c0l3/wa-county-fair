package app.core.bff;

import java.util.ArrayList;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

public class ThurFragment extends Fragment {
	static private ArrayList<SearchResults> thursEventResults = new ArrayList<SearchResults>();
	private CustomBaseAdapter adapter = null;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.thurfragment, container, false);
        
    	ListView lv2 = (ListView) view.findViewById(R.id.schedule2);
    	adapter = new CustomBaseAdapter(getActivity(), thursEventResults);
    	lv2.setAdapter(adapter);

        lv2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent detailIntent = new Intent(getActivity(), DetailViewActivity.class);

                CustomBaseAdapter ca = (CustomBaseAdapter)parent.getAdapter();
                SearchResults sr = (SearchResults)ca.getItem(position);

                detailIntent.putExtra("details", sr.getDetails());
                startActivity(detailIntent);
            }
        });

        return view;
    }

	@Override
	public void onResume() {
		super.onResume();
		SQLiteAdapter myDbHelper = new SQLiteAdapter(null);
		ArrayList<SearchResults> localEventResults = myDbHelper.getThursEvents();
		thursEventResults.clear();
		thursEventResults.addAll(localEventResults);
		adapter.notifyDataSetChanged();
	}
}