
package app.core.bff;

public class SearchResultsMap {

	private String location_description = "";
	private Double lat = (double) 0;
	private Double lon = (double) 0;
	private String category = "";
	
	public void setLocationDescription(String location_description) {
		this.location_description = location_description;
	}

	public String getLocationDescription() {
		return location_description ;
	}


	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLat() {
		return lat;
	}
	
	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Double getLon() {
		return lon;
	}
	
	public void setCat(String cat) {
		this.category = cat;
	}

	public String getCat() {
		return category;
	}
	
	
		
}