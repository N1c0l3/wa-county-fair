package app.core.bff;

import android.app.Activity;
import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ShoppingActivity extends Activity implements OnClickListener{
	static private ArrayList<SearchResults> shoppingResults = new ArrayList<SearchResults>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE); 
        setContentView(R.layout.activity_shopping);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
        
        TextView actTv = (TextView) findViewById(R.id.titleHeading);
        actTv.setText(R.string.title_activity_shopping);
        findViewById(R.id.openMenu).setOnClickListener(this);
        findViewById(R.id.openSearch).setOnClickListener(this);
        
        ListView lv1 = (ListView) findViewById(R.id.shoppinglist);
        lv1.setAdapter(new CustomAdapterShopping(this, shoppingResults));

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent detailIntent = new Intent(ShoppingActivity.this, DetailViewActivity.class);

                CustomAdapterShopping ca = (CustomAdapterShopping)parent.getAdapter();
                SearchResults sr = (SearchResults)ca.getItem(position);

                detailIntent.putExtra("details", sr.getDetails());
                startActivity(detailIntent);
            }
        });

    }
    
	@Override
	public void onResume() {
		super.onResume();
		SQLiteAdapter myDbHelper = new SQLiteAdapter(this);
		ArrayList<SearchResults> localShoppingResults = myDbHelper.getShopping();
		shoppingResults.clear();
		shoppingResults.addAll(localShoppingResults);
		ListView lv = (ListView) findViewById(R.id.shoppinglist);
		BaseAdapter adapter = (BaseAdapter)lv.getAdapter();
		adapter.notifyDataSetChanged();
	}
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    @Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.openMenu) {
			openMenuActivity();
		} else if (id == R.id.openSearch) {
			startActivity(new Intent(ShoppingActivity.this, SearchActivity.class));//open search activity, THEN open search dialog
		} else {
		}
	}
    
    private void openMenuActivity(){
		int width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
		SlideoutActivity.prepare(ShoppingActivity.this, R.id.inner_content, width);
		startActivity(new Intent(ShoppingActivity.this, MenuActivity.class));
		overridePendingTransition(0, 0);
    }
}
