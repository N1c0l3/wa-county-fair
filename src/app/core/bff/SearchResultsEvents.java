package app.core.bff;

public class SearchResultsEvents {

	private String event_description = "";
	private String day = "";
	private String start_time = "";
	private String location = "";
	
	public void setEventDesc(String event_description) {
		this.event_description = event_description;
	}

	public String getEventDesc() {
		return event_description ;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getDay() {
		return day;
	}

	public void setStartTime(String start_time) {
		this.start_time = start_time;
	}

	public String getStartTime() {
		return start_time;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}

	public String getLocation() {
		return location;
	}
	
}