package app.core.bff;

import java.util.ArrayList;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class SearchActivity extends Activity implements OnClickListener{
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE); 
        setContentView(R.layout.activity_search);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
        
        TextView actTv = (TextView) findViewById(R.id.titleHeading);
        actTv.setText(R.string.title_activity_search);
        findViewById(R.id.openMenu).setOnClickListener(this);
        findViewById(R.id.openSearch).setOnClickListener(this);
        
        onSearchRequested();
        handleIntent(getIntent());
    }
    
    @Override
    protected void onNewIntent(Intent intent) {
        // Because this activity has set launchMode="singleTop", the system calls this method
        // to deliver the intent if this activity is currently the foreground activity when
        // invoked again (when the user executes a search from this activity, we don't create
        // a new instance of this activity, so the system delivers the search intent here)
        handleIntent(intent);
    }
    
    private void handleIntent(Intent intent) {
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            // handles a click on a search suggestion; launches activity to show word
            //Intent wordIntent = new Intent(this, SearchItemActivity.class);
            //wordIntent.setData(intent.getData());
            //startActivity(wordIntent);
        	// handles a search query
            //String query = intent.getStringExtra(SearchManager.);
            //showResults(query);
        } else if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            // handles a search query
            String query = intent.getStringExtra(SearchManager.QUERY);
            showResults(query);
        }
    }
    
    private void showResults(String query) {
    	
    	/*	-- Query for all event_descriptions containing a term with the prefix contained within the variable 'query'. This will match
			-- all event_descriptions that contain, say,  "excel", but also those that contain terms "excellent",
			--"excelcior!", and so on.*/
    	//Adding a wildcard
    	String condition = "%" + query + "%";

    	//schedule
    	SQLiteAdapter myDbHelper = new SQLiteAdapter(this);
    	ArrayList<SearchResults> results = myDbHelper.getAllDayEventsWithCondition(condition, false);
    	//carnival rides
    	ArrayList<SearchResults> carnivalResults = myDbHelper.getCarnivalWithCondition(condition, false);
    	results.addAll(carnivalResults);
    	//food_vendors
    	ArrayList<SearchResults> foodResults = myDbHelper.getFoodWithCondition(condition, false);
    	results.addAll(foodResults);
    	//commercial vendors
    	ArrayList<SearchResults> shoppingResults = myDbHelper.getShoppingWithCondition(condition, false);
    	results.addAll(shoppingResults);

    	ListView lv1 = (ListView) findViewById(R.id.searchlist);
    	lv1.setAdapter(new CustomAdapterSearch(this, results));

    	lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent detailIntent = new Intent(SearchActivity.this, DetailViewActivity.class);

                CustomAdapterSearch ca = (CustomAdapterSearch)parent.getAdapter();
                SearchResults sr = (SearchResults)ca.getItem(position);

                detailIntent.putExtra("details", sr.getDetails());
                startActivity(detailIntent);
            }
        });
}
    
	@Override
	public void onResume() {
		super.onResume();
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    @Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.openMenu) {
			openMenuActivity();
		} else if (id == R.id.openSearch) {
			onSearchRequested();
		} else {
		}
	}
    
    private void openMenuActivity(){
		int width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
		SlideoutActivity.prepare(SearchActivity.this, R.id.inner_content, width);
		startActivity(new Intent(SearchActivity.this, MenuActivity.class));
		overridePendingTransition(0, 0);
    }
}
