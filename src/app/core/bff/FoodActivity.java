package app.core.bff;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView;


public class FoodActivity extends Activity implements OnClickListener{
	static private ArrayList<SearchResults> foodResults = new ArrayList<SearchResults>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_food);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
        
        TextView actTv = (TextView) findViewById(R.id.titleHeading);
        actTv.setText(R.string.title_activity_food);
        findViewById(R.id.openMenu).setOnClickListener(this);
        findViewById(R.id.openSearch).setOnClickListener(this);

        ListView lv1 = (ListView) findViewById(R.id.foodlist);
        lv1.setAdapter(new CustomAdapterFood(this, foodResults));

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent detailIntent = new Intent(FoodActivity.this, DetailViewActivity.class);

                CustomAdapterFood ca = (CustomAdapterFood)parent.getAdapter();
                SearchResults sr = (SearchResults)ca.getItem(position);

                detailIntent.putExtra("details", sr.getDetails());
                startActivity(detailIntent);
            }
        });
    }

	@Override
	public void onResume() {
		super.onResume();
		SQLiteAdapter myDbHelper = new SQLiteAdapter(this);
		ArrayList<SearchResults> localFoodResults = myDbHelper.getFood();
		foodResults.clear();
		foodResults.addAll(localFoodResults);
		ListView lv = (ListView) findViewById(R.id.foodlist);
		BaseAdapter adapter = (BaseAdapter)lv.getAdapter();
		adapter.notifyDataSetChanged();
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.openMenu) {
			openMenuActivity();
		} else if (id == R.id.openSearch) {
			startActivity(new Intent(FoodActivity.this, SearchActivity.class));//open search activity, THEN open search dialog
		} else {
		}
	}

    private void openMenuActivity(){
		int width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
		SlideoutActivity.prepare(FoodActivity.this, R.id.inner_content, width);
		startActivity(new Intent(FoodActivity.this, MenuActivity.class));
		overridePendingTransition(0, 0);
    }
}
