package app.core.bff;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;


public class SplashActivity extends Activity {

    private final int SPLASH_DISPLAY_LENGTH = 2000;
    
    DownloadFile downloadFile = new DownloadFile();
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle splash) {
        super.onCreate(splash);
        setContentView(R.layout.activity_splash);

        SQLiteAdapter myDbHelper = new SQLiteAdapter(this);       
    	myDbHelper = new SQLiteAdapter(this);
        try {
			myDbHelper.createDataBase();
		} catch (IOException ioe) {

			throw new Error("Unable to create database");
		}

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Main-Activity. */
                Intent mainIntent = new Intent(SplashActivity.this,AboutActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
            
        }, SPLASH_DISPLAY_LENGTH);
    }
    
}