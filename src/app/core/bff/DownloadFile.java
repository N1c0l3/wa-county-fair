package app.core.bff;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;


import android.os.AsyncTask;
import android.os.Environment;

public class DownloadFile extends AsyncTask<String, Integer, String>{
	@Override
	protected String doInBackground(String... sUrl) {
		try{
			URL url = new URL(sUrl[0]);
			URLConnection connection = url.openConnection();
            connection.connect();
            // this will be useful so that you can show a typical 0-100% progress bar
            int fileLength = connection.getContentLength();

            
            //This is needed to create the database directory on the first run of the WCF Application
    		File databaseDirectory = new File("/data/data/app.core.wcf/files");
    		databaseDirectory.mkdirs();
    		//This creates the empty bff.sqlite file that the buffer will write to later on
    		//File outputFile = new File(databaseDirectory, "nextbff.sqlite");
            // download the file
            InputStream input = new BufferedInputStream(url.openStream());
            OutputStream output = new FileOutputStream(databaseDirectory + "/" + "nextbff.sqlite");

            byte data[] = new byte[1024];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();
        } catch (Exception e) {
        }
        return null;
    }
}
