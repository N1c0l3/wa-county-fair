package app.core.bff;

import java.util.ArrayList;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

public class SatFragment extends Fragment {
	static private ArrayList<SearchResults> satEventResults = new ArrayList<SearchResults>();
	private CustomBaseAdapter adapter = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.satfragment, container, false);
        
    	ListView lv4 = (ListView) view.findViewById(R.id.schedule4);
    	adapter = new CustomBaseAdapter(getActivity(), satEventResults);
    	lv4.setAdapter(adapter);

        lv4.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent detailIntent = new Intent(getActivity(), DetailViewActivity.class);

                CustomBaseAdapter ca = (CustomBaseAdapter)parent.getAdapter();
                SearchResults sr = (SearchResults)ca.getItem(position);

                detailIntent.putExtra("details", sr.getDetails());
                startActivity(detailIntent);
            }
        });

        return view;
    }

	@Override
	public void onResume() {
		super.onResume();
		SQLiteAdapter myDbHelper = new SQLiteAdapter(null);
		ArrayList<SearchResults> localEventResults = myDbHelper.getSatEvents();
		satEventResults.clear();
		satEventResults.addAll(localEventResults);
		adapter.notifyDataSetChanged();
	}
}