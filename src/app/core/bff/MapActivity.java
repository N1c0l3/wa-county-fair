package app.core.bff;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.database.SQLException;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.CameraUpdateFactory;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.os.Bundle;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.TextView;
import android.widget.Toast;


public class MapActivity extends FragmentActivity implements OnClickListener{
        
        private GoogleMap gMap;
        private static final LatLng FAIRGROUNDS = new LatLng(45.53016432, -122.950008);

        static ArrayList locResults;

        public boolean isOnline() {
                boolean connected = false;
                Context Context = getApplicationContext();
                try {
                        ConnectivityManager con = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo networkInfo = con.getActiveNetworkInfo();
                connected = networkInfo != null && networkInfo.isAvailable() &&
                        networkInfo.isConnected();
                return connected;


                } catch (Exception e) {

                }
                return connected;
            }

        
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        
        setContentView(R.layout.activity_map);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
        
        findViewById(R.id.openMenu).setOnClickListener(this);
        findViewById(R.id.openSearch).setOnClickListener(this);
        
        TextView actTv = (TextView) findViewById(R.id.titleHeading);
        actTv.setText(R.string.title_activity_map);
        
        if (isOnline()) {                
        
        Toast.makeText(this,"E:  Entertainment\nL:  Livestock\nR:  Rides\nF:  Food",Toast.LENGTH_LONG).show();
        	
        gMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        gMap.setMyLocationEnabled(true);
        gMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(FAIRGROUNDS,17));//Centers the map at the WCF site, zoomed in appropriately
        
        
        ArrayList<SearchResultsMap> MapResults;
        SQLiteAdapter myDbHelper = new SQLiteAdapter(this);       
        myDbHelper = new SQLiteAdapter(this);
                
                try {
                        
                        myDbHelper.openDB();
                        MapResults = myDbHelper.getlocations();
                
                }catch(SQLException sqle){
                        throw sqle;
                }
        
                Object[] elements = MapResults.toArray();
                for (int a = 0; a < elements.length; a++) {
                        
                        String Desc = ((SearchResultsMap) elements[a]).getLocationDescription();
                        String Cat = ((SearchResultsMap) elements[a]).getCat();
                        double Lat = ((SearchResultsMap) elements[a]).getLat();
                        double Lon = ((SearchResultsMap) elements[a]).getLon();
                        
                        if ("Restrooms".equals(Desc)) {
                                gMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Lat,Lon))
                            .icon(BitmapDescriptorFactory.fromResource(R.raw.restrooms))
                        .title(Desc));
                        } else if ("Public Parking".equals(Desc)){
                                gMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Lat,Lon))
                            .icon(BitmapDescriptorFactory.fromResource(R.raw.parking))
                        .title(Desc));
                        } else if ("ATM".equals(Desc)){
                                gMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Lat,Lon))
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                        .title(Desc));
                        } else if ("First Aid".equals(Desc)){
                                gMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Lat,Lon))
                            .icon(BitmapDescriptorFactory.fromResource(R.raw.faid))
                        .title(Desc));
                        } else if ("Entertainment".equals(Cat)){
                                gMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Lat,Lon))
                            .icon(BitmapDescriptorFactory.fromResource(R.raw.ev))
                        .title(Desc));
                        } else if ("Entertainment,Activities".equals(Cat)){
                            gMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Lat,Lon))
                        .icon(BitmapDescriptorFactory.fromResource(R.raw.ev))
                        .title(Desc));
                        } else if ("Activities".equals(Cat)){
                            gMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Lat,Lon))
                        .icon(BitmapDescriptorFactory.fromResource(R.raw.ev))
                        .title(Desc));
                        } else if ("Livestock".equals(Cat)){
                                gMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Lat,Lon))
                            .icon(BitmapDescriptorFactory.fromResource(R.raw.lv))
                        .title(Desc));        
                        } else if ("Rides".equals(Cat)){
                            gMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Lat,Lon))
                        .icon(BitmapDescriptorFactory.fromResource(R.raw.ride))
                        .title(Desc));                               
                        } else if ("Transportation".equals(Cat)){
                                gMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Lat,Lon))
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                        .title(Desc));
                        } else if ("Rides".equals(Cat)){
                                gMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Lat,Lon))
                            .icon(BitmapDescriptorFactory.fromResource(R.raw.ride))
                        .title(Desc));
                        } else if ("Food".equals(Cat)){
                                gMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Lat,Lon))
                            .icon(BitmapDescriptorFactory.fromResource(R.raw.fd))
                        .title(Desc));
                        } else {        
                                gMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Lat,Lon))
                        .title(Desc));
                        }
                           
        }
    } else {
    	Toast.makeText(this,"You are not online... please check data connection",Toast.LENGTH_LONG).show();
    }
    }
    
    @Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.openMenu) {
			openMenuActivity();
		} else if (id == R.id.openSearch) {
			startActivity(new Intent(MapActivity.this, SearchActivity.class));//open search activity, THEN open search dialog
		} else {
		}
	}
    
    private void openMenuActivity(){
		int width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
		SlideoutActivity.prepare(MapActivity.this, R.id.inner_content, width);
		startActivity(new Intent(MapActivity.this, MenuActivity.class));
		overridePendingTransition(0, 0);
    }
    
}

