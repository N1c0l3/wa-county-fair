package app.core.bff;

public class SearchResultsCarnival {

	private String ride_name = "";
	private String height = "";
	private String special_rules = "";
	private String category = "";
	
	public void setRideName(String ride_name) {
		this.ride_name = ride_name;
	}

	public String getRideName() {
		return ride_name ;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getHeight() {
		return height;
	}

	public void setSpecialRules(String special_rules) {
		this.special_rules = special_rules;
	}

	public String getSpecialRules() {
		return special_rules;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategory() {
		return category;
	}
	
}
